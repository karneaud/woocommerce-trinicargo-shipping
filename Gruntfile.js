'use strict';

module.exports = function(grunt) {

	// Load NPM tasks to be used here
	grunt.loadNpmTasks( 'grunt-contrib-jshint' );
	grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
	grunt.loadNpmTasks( 'grunt-contrib-uglify' );
	grunt.loadNpmTasks( 'grunt-contrib-watch' );
	grunt.loadNpmTasks( 'grunt-contrib-copy' );
	grunt.loadNpmTasks( 'grunt-contrib-clean' );
	grunt.loadNpmTasks( 'grunt-wp-i18n' );
	grunt.loadNpmTasks( 'grunt-checktextdomain' );
	grunt.loadNpmTasks('grunt-shell');
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// Setting directory of the boilerplate to fetch files
		plugin_dir: {
			main: 'wc-trinicargo-shipping',
		},

		// Setting the assets folders
		dirs: {
			css: '<%= plugin_dir.main %>/public/css',
			js: '<%= plugin_dir.main %>/public/js',
			lang: '<%= plugin_dir.main %>/languages',
		},

		// Javascript linting with jshint.
		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			all: [
				'Gruntfile.js',
				'<%= dirs.js %>/../admin/js/*.js',
				'!<%= dirs.js %>/../admin/js/*.min.js',
				'<%= dirs.js %>/*.js',
				'!<%= dirs.js %>/*.min.js',
			]
		},

		// Minify all .js files.
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
				preserveComments: 'some'
			},
			admin: {
				files: [{
					expand: true,
					cwd: '<%= dirs.js %>/admin/js',
					src: [
						'*.js',
						'!*.min.js',
						'!Gruntfile.js',
					],
					dest: '<%= dirs.js %>/admin/',
					ext: '.min.js'
				}]
			},
			frontend: {
				files: [{
					expand: true,
					cwd: '<%= dirs.js %>/frontend/',
					src: [
						'*.js',
						'!*.min.js'
					],
					dest: '<%= dirs.js %>/frontend/',
					dest: '<%= dirs.js %>/admin/js',
					ext: '.min.js'
				}]
			},
		},

		// Minify all .css files.
		cssmin: {
			minify: {
				expand: true,
				cwd: '<%= dirs.css %>/',
				src: ['*.css', '!*.min.css'],
				dest: '<%= dirs.css %>/',
				ext: '.min.css'
			}
		},

		// Watch changes in the assets
		watch: {
			js: {
				files: [
					'<%= dirs.js %>/js/*js',
					'!<%= dirs.js %>/js/*.min.js',
					'<%= dirs.js %>/../admin/js/*.js',
					'!<%= dirs.js %>/../admin/js/*.min.js',
				],
				tasks: ['uglify']
			},
			css: {
				files: [
					'<%= dirs.css %>/../admin/css/*.css',
					'<%= dirs.css %>/*.css',
					'!<%= dirs.css %>/../admin/css/*.min.css',
					'!<%= dirs.css %>/*.min.css'
				],
				tasks: ['cssmin']
			},
			php: {
				files: [
					'<%= plugin_dir.main %>/**/*.php',
				],
				tasks: ['shell:unit_tests','copy']
			},
    	copy: {
        	files: [
            	'<%= plugin_dir.main %>/**/*',
            ]
      },
			tests: {
				files: [
					'tests/e2e-tests/*.js'
				],
				tasks: ['shell:e2e_tests']
			},
			utests: {
				files: [
					'tests/unit-tests/*.php'
				],
				tasks: ['shell:unit_tests']
			}
		},

		checktextdomain: {
			options:{
				text_domain: 'wc-trinicargo-shipping',
				keywords: [
					'__:1,2d',
					'_e:1,2d',
					'_x:1,2c,3d',
					'esc_html__:1,2d',
					'esc_html_e:1,2d',
					'esc_html_x:1,2c,3d',
					'esc_attr__:1,2d',
					'esc_attr_e:1,2d',
					'esc_attr_x:1,2c,3d',
					'_ex:1,2c,3d',
					'_n:1,2,4d',
					'_nx:1,2,4c,5d',
					'_n_noop:1,2,3d',
					'_nx_noop:1,2,3c,4d'
				]
			},
			files: {
				src:  [
					'**/*.php', // Include all files
					'!node_modules/**' // Exclude node_modules/
				],
				expand: true
			}
		},

		// Copy the plugin into the build directory
		copy: {
			main: {
				src:  ['wc-trinicargo-shipping/**/*', '!wc-trinicargo-shipping/tests'],
				dest: '../build/',
				expand: true
			}
		},

		// Clean up build directory
		clean: {

					main: {
						options: {
				      force: true
				    },
						src: ['../build/']
					}
		},

		// Compress build directory into <name>.zip and <name>-<version>.zip
		compress: {
			main: {
				options: {
					mode: 'zip',
					archive: './build/<%= pkg.name %>.zip'
				},
				expand: true,
				cwd: '../build/<%= pkg.name %>/',
				src: ['**/*'],
				dest: '<%= pkg.name %>/'
			}
		},
		shell: {
			options: {
				stdout: true,
				stderr: true
			},
			unit_tests: {
				command: '/usr/src/app/vendor/phpunit/phpunit/phpunit --debug -vvv --stderr'
			},
			e2e_tests: {
				command: 'npm run --silent test'
			}
		},

	});
	// Register Tasks
	grunt.registerTask( 'default', ['jshint', 'cssmin', 'uglify', 'copy', 'watch']);

  grunt.registerTask('test', ['shell']);
	// Build task(s).
	grunt.registerTask( 'build', [ 'clean', 'copy', 'compress' ] );

};
