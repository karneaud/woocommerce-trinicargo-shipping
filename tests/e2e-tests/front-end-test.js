import config from 'config';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import test from 'selenium-webdriver/testing';
import webdriver from 'selenium-webdriver';
import chrome from 'selenium-webdriver/chrome';
// Helper objects for performing actions.
import { WebDriverManager, WebDriverHelper as helper } from 'wp-e2e-webdriver';

// We're going to use the ShopPage and CartPage objects for this tutorial.
import { ShopPage, CartPage } from 'wc-e2e-page-objects';

chai.use( chaiAsPromised );
const assert = chai.assert;
const chromedriver = require('chromedriver');
const chromeCapabilities = webdriver.Capabilities.chrome();
chromeCapabilities.set('chromeOptions', {args: ['--headless']});
const pref = new webdriver.logging.Preferences();
pref.setLevel( 'browser', webdriver.logging.Level.SEVERE );

let manager;
let driver;

test.describe( 'Frontend Testing', function() {

    // Set up the driver and manager before testing starts.
    test.before( 'open browser', function() {
        this.timeout( config.get( 'startBrowserTimeoutMs' ) );
        driver = new webdriver.Builder()
          .usingServer( config.get("url") )
          .forBrowser('chrome')
          .withCapabilities(chromeCapabilities)
          .build();
        driver.getSession().then( function( sessionid ) {
			driver.allPassed = true;
			driver.sessionID = sessionid.id_;
		} );

        helper.clearCookiesAndDeleteLocalStorage( driver );
    } );

    this.timeout( config.get( 'mochaTimeoutMs' ) );

    // Tests will go here.
    test.it( 'Has shop page', () => {

        // Create a new Shop page object.
        const shopPage = new ShopPage( driver, { url: config.get('url').concat( '/shop' ) } );

        assert.instanceOf(shopPage, ShopPage, 'shopPage instance of ShopPage');

    } );
    // Close the browser after finished testing.
    test.after( 'quit browser', () => {
        manager.quitBrowser();
    } );

} );
