<?php
/**
 * Class SampleTest
 *
 * @package Sample_Plugin
 */

/**
 * Sample test case.
 */
class Base_Wc_Trinicargo_Shipping_Test extends WP_UnitTestCase {

	protected $order;
	protected $shipping_method;

	public function setUp()
	{
		$this->setUp_order();
		$this->setUp_shipping_method();
		$this->setUp_shipping_info();
	}

	public function test_WcGetOrder() {

	 // Assert that $order is a WC_Order object
	 $this->assertInstanceOf( 'WC_Order', $this->order );
	 // Assert that wc_get_order() accepts a WC_Order object
	 $this->assertInstanceOf( 'WC_Order', wc_get_order( $this->order ) );
 }

 protected function setUp_order()
 {
	 $product = WC_Helper_Product::create_simple_product();
	 $product->set_weight(rand(5,20));
	 $product->set_height(rand(5,20));
	 $product->set_length(rand(5,20));
	 $product->set_width(rand(5,30));
	 $product->set_description('Test description');
	 $product->save();
	 $this->order = WC_Helper_Order::create_order(1, $product);
 }

 protected function setUp_shipping_method()
 {
	 $this->shipping_method = $this->getMockBuilder(Wc_Trincargo_Shipping_Method::class)
					 ->setMethods(['create_waybill'])
					 ->getMock();
	 $this->shipping_method->settings['waybill_pickupdays'] = round(rand(2,7));
	 $this->shipping_method->settings['waybill_customer_id']= uniqid();
	 $this->shipping_method->settings['waybill_username'] = 'carigamers';
	 $this->shipping_method->settings['waybill_password'] = 'carigamers.com';
	 $this->shipping_method->settings['waybill_pickupcosignee'] = get_option('woocommerce_email_from_name');
	 $this->shipping_method->settings['waybill_pickupaddress'] = get_option('woocommerce_store_address');
	 $this->shipping_method->settings['waybill_pickupaddress1'] = get_option('woocommerce_store_address1');
	 $this->shipping_method->settings['waybill_pickupcity'] = get_option('woocommerce_store_city');
	 add_action('woocommerce_order_status_processing', [$this->shipping_method, 'create_waybill'], 10, 1 );

 }

 protected function setUp_address_info()
 {
	 $address_info = new AddressInfo;
	 $address_keys = array_keys($address_info->__toArray());
	 $shipping = $this->order->get_data()['billing'];
	 $params = array_intersect_key(
							 $shipping,
							 array_flip([
								 'address_1',
								 'address_2',
								 'city',
								 'state',
								 'postcode',
								 'phone'
							 ])
					 );
	 $params = array_values($params);
	 $params = array_combine($address_keys, array_merge(
			 [
					 sprintf("%s %s", $shipping['first_name'], $shipping['last_name'])
			 ],
			 $params,
			 ['TTO']
	 ));

	 $address_info->set_parameters($params);

	 return $address_info;
 }

 protected function setUp_shipping_info()
 {
 		$billing = $this->order->get_data()['billing'];
		foreach ($billing as $key => $value) {
			if(method_exists($this->order, "set_shipping_${key}"))
				call_user_func([$this->order, "set_shipping_${key}"], $value);
		}
 }

 public function tearDown()
 {
 		remove_action('woocommerce_order_status_processing', array($this->shipping_method, 'create_waybill'), 10, 1 );
 }

}
