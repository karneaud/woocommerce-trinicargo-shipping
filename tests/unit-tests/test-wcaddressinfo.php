<?php
/**
 * Class SampleTest
 *
 * @package Sample_Plugin
 */

/**
 * Sample test case.
 */
class Wc_Address_Info_Test extends Base_Wc_Trinicargo_Shipping_Test {

	protected $address_info;

	public function setUp()
	{
		parent::setUp();

		$this->address_info = new AddressInfo;
		$address_keys = array_keys($this->address_info->__toArray());
		$shipping = $this->order->get_data()['billing'];
		$params = array_intersect_key(
								$shipping,
								array_flip([
								'address_1',
								'address_2',
								'city',
								'state',
								'postcode',
								'phone'
								])
						);
		$params = array_values($params);
		$params = array_combine($address_keys, array_merge(
				[
						sprintf("%s %s", $shipping['first_name'], $shipping['last_name'])
				],
				$params,
				['TTO']
		));

		$this->address_info->set_parameters($params);
	}

	public function test_AddressLine1($value='')
	{
		$shipping = $this->order->get_data()['billing'];
		$this->assertEquals(
			$shipping['address_1'],
			$this->address_info->AddressLine1
		);
	}

	public function test_AddressInfoName($value='')
	{
		$shipping = $this->order->get_data()['billing'];
		$this->assertContains($shipping['first_name'], $this->address_info->Name);
	}

}
