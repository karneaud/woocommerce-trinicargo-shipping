<?php
/**
* Class SampleTest
*
* @package Sample_Plugin
*/

/**
* Sample test case.
*/
class Wc_Trinicargo_Shipping_Method_Create_Waybill_Test extends Base_Wc_Trinicargo_Shipping_Test {

	protected $create_waybill;

	public function setUp()
	{
		parent::setUp();
		$this->setUp_create_waybill();
	}

	public function test_CreateWayBillInitMethodCalled()
	{
		 $this->create_waybill->expects($this->once())->method('init')->with($this->equalTo($this->order));
		 $result = $this->order->payment_complete(uniqid());
		 do_action('wc-trinicargo-shipping_create_waybill', $this->order, $this->shipping_method->settings);
		 $this->assertGreaterThan(0, did_action('woocommerce_order_status_processing'));
		 $this->assertEquals(1, did_action('wc-trinicargo-shipping_create_waybill'));

	}

	protected function setUp_create_waybill()
	{
		$this->create_waybill = $this->getMockBuilder(Wc_Trinicargo_Shipping_Create_Waybill::class)
 					 ->setMethods(['init','update_order'])
 					 ->getMock();

		add_action('wc-trinicargo-shipping_create_waybill', [ $this->create_waybill , 'init'], 1, 2);
	}

	public function tearDown()
	{
		parent::tearDown();
		remove_action('wc-trinicargo-shipping_create_waybill', array($this->create_waybill, 'init'), 1, 2 );
	}
}
