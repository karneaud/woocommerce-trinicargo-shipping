<?php
/**
 * Class SampleTest
 *
 * @package Sample_Plugin
 */

/**
 * Sample test case.
 */
class Wc_Waybill_Test extends Base_Wc_Trinicargo_Shipping_Test {

	protected $waybill;

	public function setUp()
	{
		parent::setUp();

		$this->waybill = new Waybill;
		$this->create_waybill();
	}

	private function create_waybill()
	{
			$date = new DateTime();
			$address_info = $this->setUp_address_info();
			$item = $this->order->get_items();
			$product = current($item)->get_product();
			$this->waybill->set_parameters(
				[
					'ActualWeight' => $product->get_weight(),
					'Height' => $product->get_height(),
					'UnitDimensions' => get_option('woocommerce_dimensions_unit'),
					'WeightUnit' => get_option('woocommerce_weight_unit'),
					'PickupRequestDate'=> $date->add(new DateInterval('P2D'))->format('Y-m-d')
				]
			);
	}

	public function test_WayBillParameters($value='')
	{
		$this->assertEquals(get_option('woocommerce_weight_unit'), 'kgs');
		$this->assertEquals(get_option('woocommerce_weight_unit'), $this->waybill->WeightUnit);
		$this->assertEquals(current($this->order->get_items())->get_product()->get_height(), $this->waybill->Height);
	}

}
