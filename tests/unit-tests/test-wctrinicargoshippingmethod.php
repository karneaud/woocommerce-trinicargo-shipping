<?php
/**
* Class SampleTest
*
* @package Sample_Plugin
*/

/**
* Sample test case.
*/
class Wc_Trinicargo_Shipping_Method_Test extends Base_Wc_Trinicargo_Shipping_Test {

protected $shipping_method;

public function setUp()
{
	parent::setUp();
}

public function test_OrderStatus()
{
	$this->assertFalse($this->order->is_paid());
	$this->assertFalse($this->order->has_status('processing'));
	$this->assertEquals(0, did_action('woocommerce_order_status_processing'));
}

 public function test_CreateWayBillMethodCalled()
 {
 		$this->shipping_method->expects($this->once())->method('create_waybill')->with($this->equalTo($this->order->get_id()));
 		$result = $this->order->payment_complete(uniqid());
		$this->assertEquals(1, did_action('woocommerce_order_status_processing'));
		$this->assertTrue($result);
 }

}
