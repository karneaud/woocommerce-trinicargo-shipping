<?php
/**
 * Class SampleTest
 *
 * @package Sample_Plugin
 */

/**
 * Sample test case.
 */
class Wc_Trincargo_Shipping_Options_Test extends Base_Wc_Trinicargo_Shipping_Test {

	protected $shipping_method;

	public function setUp()
	{
		parent::setUp();

		$this->shipping_method = new Wc_Trincargo_Shipping_Method(12);
		$this->shipping_method->settings = [
						'waybill_pickupdays' => round(rand(2,7)),
						'waybill_customer_id' =>  uniqid(),
						'waybill_username' => 'carigamers',
						'waybill_password' => 'carigamers.com',
						'waybill_pickupcosignee' => get_option('woocommerce_email_from_name'),
						'waybill_pickupaddress' => get_option('woocommerce_store_address'),
						'waybill_pickupaddress1' => get_option('woocommerce_store_address1'),
						'waybill_pickupcity' => get_option('woocommerce_store_city')];

	}

	public function test_ShippingMethodSettings()
	{
			$this->assertNotNull($this->shipping_method->settings);
			$this->assertEquals($this->shipping_method->settings['waybill_username'], 'carigamers', print_r($this->shipping_method->settings, true));
			$this->assertNotNull($this->shipping_method->get_option('waybill_pickupcity'));
			$this->assertEquals($this->shipping_method->get_option('waybill_pickupcity'), 'San Fernando');
	}

}
