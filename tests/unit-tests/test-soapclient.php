<?php /**
 *
 */
class Wc_SoapClient_Test extends  WP_UnitTestCase
{
    protected $client;

    public function setUp()
    {
        parent::setUp();

        $this->client = new SoapClient( plugin_dir_path( dirname( __FILE__ ) ) . '../wc-trinicargo-shipping/includes/wsdl/WayBill.wsdl' , [ "trace" => true, "exceptions" => false ] );
    }

    public function test_CreateWayBillSoapFault()
    {
        $result = $this->client->__soapCall('CreateWayBill', ['WayBill' => []], [$this->client->__getLastRequest()]);
        $this->assertInstanceOf(SoapFault::class, $result);
    }
}
 ?>
